# Cineca Leonardo
#                                                         
# Your environment was identified as running on the Cineca Leonardo platform.
# To successfully compile you will also need access to the following modules
# and environment variables.
#
# # Necessary module commands:
# module load profile/global
# module load intel-oneapi-compilers intel-oneapi-mpi intel-oneapi-mkl
# module load boost
# module load hdf5/1.14.3--intel-oneapi-mpi--2021.10.0--oneapi--2023.2.0
#
# # Necessary environment variables:
# export CC=${MPICC}
# export CXX=${MPICXX}
# export FC=${MPIFC}
# export FF=${MPIF77}
# export F77=${MPIF77}
# export F90=${MPIF90}
#
# export BOOST_DIR=${BOOST_HOME}
# export HDF5_DIR=${HDF5_HOME}
#
# Consider adding all the previous lines to your
# ${HOME}/.bashrc file (or equivalent).
##

MKL_DIR=${INTEL_ONEAPI_MKL_HOME}
BLAS_DIR=${INTEL_ONEAPI_MKL_HOME}/lib/intel64/
LAPACK_DIR=${INTEL_ONEAPI_MKL_HOME}/lib/intel64/

BLAS_LIBRARY_NAMES="libmkl_core.so;libmkl_sequential.so"
LAPACK_LIBRARY_NAMES="libmkl_intel_lp64.so"

# Try to disable fast math (OneAPI compilers have it enabled by default).
export OPTIMIZATION_FLAGS="-fno-fast-math"
export CFLAGS="-fno-fast-math"
export CXXFLAGS="-fno-fast-math"

PACKAGES="once:zlib once:cmake once:tbb ${PACKAGES}"

# Remove HDF5 from package list.
PACKAGES=$(echo ${PACKAGES} | sed "s/\(once\|load\|skip\):\(hdf5\)//g")

# Explicitly locate Boost for deal.II to find.
BOOST_DIR=/leonardo_work/IscrB_HPCHeart/lifex-env/boost-1.76.0
