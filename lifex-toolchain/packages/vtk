VERSION_MAJOR=9.0
VERSION_MINOR=3
VERSION=${VERSION_MAJOR}.${VERSION_MINOR}
CHECKSUM=1abe6e2d7988193cf7d64c4d84287956

SOURCE=https://www.vtk.org/files/release/${VERSION_MAJOR}/
NAME=VTK-${VERSION}
EXTRACTSTO=VTK-${VERSION}
PACKING=.tar.gz

BUILDCHAIN=cmake

INSTALL_PATH=${INSTALL_PATH}/${NAME}

#########################################################################
# Please do not change the following options

# Set compilers & compiler options
if [ ! -z "${CC}" ]; then
    CONFOPTS="\
        ${CONFOPTS} \
        -DCMAKE_C_COMPILER=${CC}"
fi

if [ ! -z "${CXX}" ]; then
    CONFOPTS="\
        ${CONFOPTS} \
        -DCMAKE_CXX_COMPILER=${CXX}"
fi

if [ ! -z "${FC}" ]; then
    CONFOPTS="\
        ${CONFOPTS} \
        -DCMAKE_Fortran_COMPILER=${FC}"
fi

CONFOPTS="-DCMAKE_BUILD_TYPE:STRING=RELEASE \
          -DBUILD_SHARED_LIBS:BOOL=ON \
          -DCMAKE_INSTALL_RPATH:STRING=${INSTALL_PATH}/lib \
          -DBUILD_TESTING:BOOL=OFF \
          -DVTK_EXTRA_COMPILER_WARNINGS:BOOL=OFF \
          -DVTK_GROUP_ENABLE_Imaging:STRING=DONT_WANT \
          -DVTK_GROUP_ENABLE_MPI:STRING=DONT_WANT \
          -DVTK_GROUP_ENABLE_Qt:STRING=DONT_WANT \
          -DVTK_GROUP_ENABLE_Rendering:STRING=DONT_WANT \
          -DVTK_GROUP_ENABLE_StandAlone:STRING=WANT \
          -DVTK_GROUP_ENABLE_Views:STRING=DONT_WANT \
          -DVTK_GROUP_ENABLE_Web:STRING=DONT_WANT \
          -DVTK_USE_MPI:BOOL=OFF \
          -DVTK_WRAP_JAVA:BOOL=OFF \
          -DVTK_WRAP_PYTHON:BOOL=OFF"

if [ ! -z "${OPTIMIZATION_FLAGS}" ]; then
    CONFOPTS="${CONFOPTS} \
      -DCMAKE_CXX_FLAGS=\"${OPTIMIZATION_FLAGS}\""
fi

# Disable warnings for pre-C99 features.
CONFOPTS="${CONFOPTS} \
      -DCMAKE_C_FLAGS=\"-Wno-implicit-function-declaration -Wno-incompatible-function-pointer-types\""

package_specific_register() {
    export VTK_DIR=${INSTALL_PATH}
}

package_specific_conf() {
    # Generate configuration file
    CONFIG_FILE=${CONFIGURATION_PATH}/${NAME}
    rm -f $CONFIG_FILE
    echo "
export VTK_DIR=${INSTALL_PATH}
" >> $CONFIG_FILE

    echo
    echo "${NAME} has now been installed in"
    echo
    cecho ${GOOD} "    ${INSTALL_PATH}"
    echo
    echo "To update your environment variables, execute the following command:"
    echo
    cecho ${GOOD} "    source $CONFIG_FILE"
    echo
}
