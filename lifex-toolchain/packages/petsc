VERSION=3.22.2
CHECKSUM=cc048d53d9fa05ff14fef31ba424f1cf

NAME=petsc-lite-${VERSION}
SOURCE=https://gitlab.com/petsc/petsc/-/archive/v${VERSION}/
PACKING=.tar.gz

EXTRACTSTO=petsc-v${VERSION}-2a89477b25f3695626c03c30fbd331171623fac3
BUILDCHAIN=custom

INSTALL_PATH=${INSTALL_PATH}/petsc-${VERSION}

#########################################################################

CONFOPTS="
  --with-debugging=0
  --with-shared-libraries=1
  --with-mpi=1
  --with-x=0
  --with-64-bit-indices=0
"

if [ ! -z "${OPTIMIZATION_FLAGS}" ]; then
    CONFOPTS="${CONFOPTS} \
      COPTFLAGS=${OPTIMIZATION_FLAGS} \
      CXXOPTFLAGS=${OPTIMIZATION_FLAGS}"
fi


for external_pkg in hypre mumps; do
    CONFOPTS="${CONFOPTS} --download-${external_pkg}=1"
done

if [ ! -z "$CC" ]; then
    CONFOPTS="${CONFOPTS} CC=$CC"
fi

if [ ! -z "$CXX" ]; then
    CONFOPTS="${CONFOPTS} CXX=$CXX"
fi

if [ ! -z "$FC" ]; then
    CONFOPTS="${CONFOPTS} FC=$FC"
fi

if [ ! -z "$F77" ]; then
    CONFOPTS="${CONFOPTS} F77=$F77"
fi

if [ ! -z "$F90" ]; then
    CONFOPTS="${CONFOPTS} F90=$F90"
fi

if [ ! -z "$CFLAGS" ]; then
    CONFOPTS="${CONFOPTS} CFLAGS=$CFLAGS"
fi

if [ ! -z "$CXXFLAGS" ]; then
    CONFOPTS="${CONFOPTS} CXXFLAGS=$CXXFLAGS"
fi

if [ ! -z "${MKL_DIR}" ]; then
    cecho ${INFO} "PETSc: configuration with MKL"
    cecho ${INFO} "PETSc: configuration with blas-lapack-dir=${MKL_DIR}"
    CONFOPTS="${CONFOPTS} --with-blas-lapack-dir=${MKL_DIR}"
else
    if [ ! -z "${BLAS_DIR}" ]; then
        cecho ${INFO} "PETSc: configuration with --with-blas-lib=${BLAS_DIR}/libblas.a"
        CONFOPTS="${CONFOPTS} --with-blas-lib=${BLAS_DIR}/libblas.a"
    fi
    if [ ! -z "${LAPACK_DIR}" ]; then
        cecho ${INFO} "PETSc: configuration with --with-lapack-lib=${LAPACK_DIR}/liblapack.a"
        CONFOPTS="${CONFOPTS} --with-lapack-lib=${LAPACK_DIR}/liblapack.a"
    fi
fi

# Add ParMETIS, if present
if [ ! -z "${PARMETIS_DIR}" ]; then
    cecho ${INFO} "PETSc: configuration with ParMETIS"
    CONFOPTS="\
        ${CONFOPTS} \
        --with-parmetis-dir=${PARMETIS_DIR} \
        --with-metis-dir=${PARMETIS_DIR}"

    for external_pkg in scalapack mumps; do
        CONFOPTS="${CONFOPTS} --download-${external_pkg}=1"
    done
fi

#########################################################################

package_specific_setup() {
    cd ${BUILDDIR}
    cp -rf ${SRC_PATH}/${EXTRACTSTO}/* .

    # make sure no other invalid PETSC_DIR is set:
    unset PETSC_DIR

    INTEL_FLAGS_OLD="${__INTEL_PRE_CFLAGS}"

    # These flags are needed when using Intel OneAPI, to:
    # - disable 'unused command line argument' warnings (which prevent
    #   the build system from identifying the compiler in some cases);
    # - allow compiling pre-C99 code (apparently some of it is part of
    #   Scalapack) with a C99-compilant compiler.
    export __INTEL_PRE_CFLAGS="-w -Wno-implicit-function-declaration -Wno-implicit-int -Wno-incompatible-function-pointer-types ${INTEL_FLAGS_OLD}"
    
    python3 ./configure --prefix=${INSTALL_PATH} ${CONFOPTS}
    quit_if_fail "petsc ./configure failed"
    
    make all install
    quit_if_fail "petsc make all install failed"

    # Restore old Intel flags.
    export __INTEL_PRE_CFLAGS="${INTEL_FLAGS_OLD}"
}

package_specific_register() {
    export PETSC_DIR=${INSTALL_PATH}
    if [ ! -z "${PARMETIS_DIR}" ]; then
        export SCALAPACK_DIR=${INSTALL_PATH}
    fi
}

package_specific_conf() {
    # Generate configuration file
    CONFIG_FILE=${CONFIGURATION_PATH}/petsc-${VERSION}
    rm -f $CONFIG_FILE
    echo "
export PETSC_DIR=${INSTALL_PATH}
" >> $CONFIG_FILE
    if [ ! -z "${PARMETIS_DIR}" ]; then
       echo "export SCALAPACK_DIR=${INSTALL_PATH}" >> $CONFIG_FILE
    fi
}
