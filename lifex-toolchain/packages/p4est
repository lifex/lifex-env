VERSION=2.3.2
CHECKSUM=0ea6e4806b6950ad64e62a5607bfabbb

NAME=p4est-$VERSION
EXTRACTSTO=p4est-${VERSION}
SOURCE=https://p4est.github.io/release/
PACKING=.tar.gz
BUILDCHAIN=custom

INSTALL_PATH=${INSTALL_PATH}/${NAME}

package_specific_setup() {
    function bdie() {
        echo "Error: $@"
        exit 1
    }

    # export zlib if we installed it ourselves
    if [ ! -z "${ZLIB_DIR}" ]; then
        if [ -z "${ZLIB_LIBPATH}" ]; then
            ZLIB_LIBPATH=${ZLIB_DIR}/lib
        fi
        LIBS="-L${ZLIB_LIBPATH}"

        if [ -z "${ZLIB_INCLUDE}" ]; then
            ZLIB_INCLUDE=${ZLIB_DIR}/include
        fi
        CFLAGS="$CFLAGS -I${ZLIB_INCLUDE}"
    fi

    if test "x$CFLAGS" = x && test "x$P4EST_CFLAGS_FAST" = x ; then
        export CFLAGS_FAST="-O3"
    else
        export CFLAGS_FAST="$CFLAGS $P4EST_CFLAGS_FAST"
    fi

    if test "x$CFLAGS" = x && test "x$P4EST_CFLAGS_DEBUG" = x ; then
        export CFLAGS_DEBUG="-O0 -g"
    else
        export CFLAGS_DEBUG="$CFLAGS $P4EST_CFLAGS_DEBUG"
    fi

    # choose names for fast and debug build directories
    BUILD_FAST="${BUILD_PATH}/${NAME}/FAST"
    BUILD_DEBUG="${BUILD_PATH}/${NAME}/DEBUG"
    
    # choose names for fast and debug installation directories
    INSTALL_FAST="${INSTALL_PATH}/FAST"
    INSTALL_DEBUG="${INSTALL_PATH}/DEBUG"
    
    echo
    echo "This script tries to configure and build the p4est library."
    echo
    echo "CFLAGS_FAST: $CFLAGS_FAST"
    echo "Build FAST: $BUILD_FAST"
    echo "Install FAST: $INSTALL_FAST"
    echo "Checking environment: CFLAGS P4EST_CFLAGS_FAST"
    echo "See output files $BUILD_FAST/config.output and make.output"
    echo
    echo "CFLAGS_DEBUG: $CFLAGS_DEBUG"
    echo "Build DEBUG: $BUILD_DEBUG"
    echo "Install DEBUG: $INSTALL_DEBUG"
    echo "Checking environment: CFLAGS P4EST_CFLAGS_DEBUG"
    echo "See output files $BUILD_DEBUG/config.output and make.output"
    echo
    
    test -f "${SRC_PATH}/${EXTRACTSTO}/src/p4est.h" || bdie "Main header file missing"
    test -f "${SRC_PATH}/${EXTRACTSTO}/configure" || bdie "Configure script missing"
    
    # remove old versions
    if test -d "${BUILD_PATH}/${NAME}"; then
        rm -rf "${BUILD_PATH}/${NAME}"
        quit_if_fail "p4est: rm -rf ${BUILD_PATH}/${NAME} failed"
    fi
    
    echo "Build FAST version in $BUILD_FAST"
    mkdir -p "$BUILD_FAST"
    cd "$BUILD_FAST"
    "${SRC_PATH}/${EXTRACTSTO}/configure" --enable-mpi --enable-shared \
        --disable-vtk-binary --without-blas \
        --prefix="$INSTALL_FAST" CFLAGS="$CFLAGS_FAST" \
        CPPFLAGS="-DSC_LOG_PRIORITY=SC_LP_ESSENTIAL" F77="$FF" \
        "$@" > config.output || bdie "Error in configure"
    make -C sc -j${PROCS} > make.output || bdie "Error in make sc"
    make -j${PROCS} >> make.output || bdie "Error in make p4est"
    make install >> make.output || bdie "Error in make install"
    grep -q "#define P4EST_HAVE_ZLIB" $INSTALL_FAST/include/p4est_config.h || bdie "Error: p4est couldn't find zlib"
    echo "FAST version installed in $INSTALL_FAST"

    echo
    echo "Build DEBUG version in $BUILD_DEBUG"
    mkdir -p "$BUILD_DEBUG"
    cd "$BUILD_DEBUG"
    "${SRC_PATH}/${EXTRACTSTO}/configure" --enable-debug --enable-mpi --enable-shared \
        --disable-vtk-binary --without-blas \
        --prefix="$INSTALL_DEBUG" CFLAGS="$CFLAGS_DEBUG" \
        CPPFLAGS="-DSC_LOG_PRIORITY=SC_LP_ESSENTIAL" F77="$FF" \
        "$@" > config.output || bdie "Error in configure"
    make -C sc -j${PROCS} > make.output || bdie "Error in make sc"
    make -j${PROCS} >> make.output || bdie "Error in make p4est"
    make install >> make.output || bdie "Error in make install"
    grep -q "#define P4EST_HAVE_ZLIB" $INSTALL_DEBUG/include/p4est_config.h || bdie "Error: p4est couldn't find zlib"
    echo "DEBUG version installed in $INSTALL_DEBUG"
    echo
    
    cd "${BUILD_PATH}/${NAME}"
    touch lifex-env_successful_build
}

package_specific_register() {
    export P4EST_DIR=${INSTALL_PATH}
}

package_specific_conf() {
    # Generate configuration file
    CONFIG_FILE=${CONFIGURATION_PATH}/${NAME}
    rm -f $CONFIG_FILE
    echo "
export P4EST_DIR=${INSTALL_PATH}
" >> $CONFIG_FILE
}
