# Build with:
# docker build -t registry.gitlab.com/lifex/lifex-env .
# docker tag registry.gitlab.com/lifex/lifex-env registry.gitlab.com/lifex/lifex-env:2024.0
# Then push with:
# docker login registry.gitlab.com
# docker push registry.gitlab.com/lifex/lifex-env:latest
# docker push registry.gitlab.com/lifex/lifex-env:2024.0


FROM debian:buster AS lifex-env_builder

MAINTAINER pafrica@sissa.it


# Install dependencies.
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y && \
    apt-get upgrade -y && \
    apt-get install -y build-essential lsb-release \
      wget curl emacs vim nano \
      automake autoconf libtool cmake git subversion \
      gfortran python3 python3-distutils python3-numpy python3-pip \
      libblas-dev liblapack-dev libblas3 liblapack3 \
      openssh-client zip unzip zlib1g-dev \
      lcov clang-format cppcheck doxygen graphviz jabref

# Create alias "python".
RUN ln -fs /usr/bin/python3 /usr/bin/python


# Clone and build lifex.
RUN git clone https://gitlab.com/lifex/lifex-env.git /lifex-env-src

WORKDIR /lifex-env-src
RUN ./lifex-env.sh --prefix=/opt/lifex-env -j6 -y --platform=lifex-toolchain/platforms/docker

# Cleanup.
RUN rm -rf /lifex-env-src /opt/lifex-env/tmp

# Install python-gitlab.
RUN /bin/bash -c \
    "source /opt/lifex-env/configuration/enable_lifex.sh && \
     python -m pip install --upgrade pip && \
     pip install python-gitlab"

# Initialize JabRef.
RUN jabref -n -b


FROM scratch AS lifex-env
COPY --from=lifex-env_builder / /

ENV HOME /root

# Enable lifex-env by default.
RUN printf "\n# lifex-env.\n\
source /opt/lifex-env/configuration/enable_lifex.sh\n" >> ${HOME}/.bashrc

# Set configuration variables.
USER root
WORKDIR ${HOME}
