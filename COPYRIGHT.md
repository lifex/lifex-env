# Table of contents

- [**<kbd>life<sup>x</sup>-env</kbd>** copyright and license](#copyright-license)
- [Contacts](#contacts)


<a name="copyright-license"></a>
# **<kbd>life<sup>x</sup>-env</kbd>** copyright and license

This directory contains the **<kbd>life<sup>x</sup>-env</kbd>** library.

The **<kbd>life<sup>x</sup>-env</kbd>** library is copyrighted by the
**<kbd>life<sup>x</sup>-env</kbd>** authors. This term refers to the people
listed in the [`AUTHORS.md`](AUTHORS.md) file and in the
[list of contributors](https://gitlab.com/lifex/lifex-env/-/graphs/main)
to the repository, recorded by the version control system
[<kbd>git</kbd>](https://git-scm.com/) as commit authors.

**<kbd>life<sup>x</sup>-env</kbd>** is free software; you can use it,
redistribute it, and/or modify it under the terms of the
<b>GNU General Public License</b> as published by the
Free Software Foundation, either <b>version 3</b> of the License,
or (at your option) any later version.

The full text of the license is quoted in the [`LICENSE.md`](LICENSE.md) file.

**<kbd>life<sup>x</sup>-env</kbd>** has been readapted from <kbd>candi</kbd>,
available at its [development repository](https://github.com/dealii/candi)
and covered by the same license.


<a name="contacts"></a>
# Contacts

For further questions concerning licensing, distribution and use please contact the
**<kbd>life<sup>x</sup>-env</kbd>** principal developers or project administrators directly:
- Pasquale Claudio Africa (principal author and project administrator)
[<kbd>pasqualeclaudio.africa@polimi.it</kbd>](mailto:pasqualeclaudio.africa@polimi.it)
